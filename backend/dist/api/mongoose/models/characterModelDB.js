"use strict";
var __importStar = (this && this.__importStar) || function (mod) {
    if (mod && mod.__esModule) return mod;
    var result = {};
    if (mod != null) for (var k in mod) if (Object.hasOwnProperty.call(mod, k)) result[k] = mod[k];
    result["default"] = mod;
    return result;
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose = __importStar(require("mongoose"));
const Schema = mongoose.Schema;
exports.CharacterModelName = "Character";
exports.CharacterSchema = new Schema({
    age: {
        age: {
            type: mongoose.Schema.Types.Number,
        },
        name: {
            type: mongoose.Schema.Types.String,
        },
    },
    image: {
        type: mongoose.Schema.Types.String,
    },
    name: {
        type: mongoose.Schema.Types.String,
    },
    pagerank: {
        rank: {
            type: mongoose.Schema.Types.Number,
        },
        title: {
            type: mongoose.Schema.Types.String,
        },
    },
    // tslint:disable-next-line:object-literal-sort-keys
    gender: {
        type: mongoose.Schema.Types.String,
    },
    slug: {
        type: mongoose.Schema.Types.String,
    },
    house: {
        type: mongoose.Schema.Types.String,
    },
    books: [
        { type: mongoose.Schema.Types.String },
    ],
    titles: [
        { type: mongoose.Schema.Types.String },
    ],
});
const CharacterModelDB = mongoose.model("Character", exports.CharacterSchema);
exports.CharacterModelDB = CharacterModelDB;
//# sourceMappingURL=characterModelDB.js.map