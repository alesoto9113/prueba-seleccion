import * as React from "react";
import { NavLink } from "react-router-dom";
import styled from "../../utils/styled";
import Container from "./Container";

interface IHeaderProps {
  title: string;
}

const Header: React.SFC<IHeaderProps> = ({ title }) => (
  <Wrapper>
    <HeaderInner>
      <HeaderLeft>
        <Title>{title}</Title>
      </HeaderLeft>
      <HeaderNav>
        <HeaderNavLink to="/">Home</HeaderNavLink>
        <HeaderNavLink to="/characters">Characters</HeaderNavLink>
      </HeaderNav>
    </HeaderInner>
  </Wrapper>
)

const Wrapper = styled("header")`
  padding: 0.5rem 1.5rem;
  background-color: #eb5558;
  color: #fefefe;
  font-family: "'IBM Plex Sans', -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell,  Fira Sans, Droid Sans, Helvetica Neue, Arial, sans-serif";
  margin-bottom: 10px;
`;

const HeaderInner = styled(Container)`
  display: flex;
  flex-direction: column;
  align-items: center;
  justify-content: space-between;

  @media (min-width: 992px) {
    flex-direction: row;
  }
`;

const HeaderLeft = styled("div")`
  padding-right: 1rem;
`

const HeaderNav = styled("nav")`
  flex: 1 1 auto;
  margin: 1rem 0;

  @media (min-width: 992px) {
    margin: 0;
  }
`;

const HeaderNavLink = styled(NavLink)`
  margin: 0 1rem;
`;

const Title = styled("h2")`
  margin: 0;
  font-weight: 500;
`;

export default Header;
