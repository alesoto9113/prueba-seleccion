import { injectGlobal } from "react-emotion";
import globals from "./globals";
import normalize from "./normalize";

// tslint:disable-next-line:no-unused-expression
injectGlobal`
  ${normalize}
  ${globals}
`;
