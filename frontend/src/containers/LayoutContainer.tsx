import * as React from "react";
import { ReactReduxContext } from "react-redux";

interface ILayoutContainerRenderProps {
  children?: any;
}

const LayoutContainer: React.FC<ILayoutContainerRenderProps> = ({ children }) => {
  return (
    <ReactReduxContext.Consumer>
      {({ }) => {
        if (children) {
          return children();
        }
      }}
    </ReactReduxContext.Consumer>
  )
}

export default LayoutContainer;
