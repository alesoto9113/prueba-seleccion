import axios, { AxiosResponse } from "axios";
import {NextFunction, Request, Response} from "express";
import httpStatus from "http-status";
import _ from "lodash";
import HttpException from "../exceptions/HttpException";
import {CharacterControllerDB} from "../mongoose/controllers/characterControllerDB";

const db = new CharacterControllerDB();
class CharacterController {

    public static get getInstance(): CharacterController {
        return this.instance || (this.instance = new this());
    }

    public static listCharacters = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await db.list(10,
                (req && req.query && req.query.page ? req.query.page : 0),
                (req && req.query && req.query.text ? req.query.text : ""));
            res.json(data);
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json(new HttpException(httpStatus.BAD_REQUEST, "Error"));
        }
    }

    public static findCharacterById = async (req: Request, res: Response, next: NextFunction) => {
        try {
            const data = await db.find(req && req.params && req.params.id ? req.params.id : "");
            res.json(data);
        } catch (error) {
            res.status(httpStatus.BAD_REQUEST).json(new HttpException(httpStatus.BAD_REQUEST, "Error"));
        }
    }

    public static getGotApiData = async (req: Request, res: Response, next: NextFunction) => {
        axios.get("https://api.got.show/api/general/characters")
        .then( async (response: AxiosResponse<any>) => {
            const {data}: any = response;
            const unionData: any = _.unionBy(data.book, data.show, "name");
            await db.dropCollection();
            await Promise.all(unionData.map((value: ICharacter, index: any) => {
                return db.create(value);
            }));
            res.json(data);
        })
        .catch((error) => {
            res.status(httpStatus.BAD_REQUEST).json(new HttpException(httpStatus.BAD_REQUEST, "Error"));
        });
    }

    private static instance: CharacterController;

}

export default CharacterController;
