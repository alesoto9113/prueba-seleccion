import * as React from "react";
import { ICharacter } from "../store/characters/types";
import styled from "../utils/styled";

export interface IDetailViewProps {
  character: ICharacter;
}

const DetailView: React.SFC<IDetailViewProps> = ({ character }) => {
  return (
    <Wrapper>
      {character && (
        <CharacterInfobox>
          <CharacterInfoboxBlurBackground src={character.img} />
          <CharacterInfoboxInner>
            <CharacterInfoboxImage src={character.image} />
            <CharacterInfoboxHeading>
              <CharacterName2>{character.name}</CharacterName2>
              <CharacterDetails>
                {character.attack_type} Slug:{" "}
                <span>{character.slug}</span>
              </CharacterDetails>
              <CharacterDetails>
                {character.attack_type} Gender:{" "}
                <span>{character.gender}</span>
              </CharacterDetails>
              <CharacterDetails>
                {character.attack_type} Age:{" "}
                <span>{character.age ? character.age.age : "-"}</span>
              </CharacterDetails>
              <CharacterDetails>
                {character.attack_type} Rank:{" "}
                <span>
                  {character.pagerank ? character.pagerank.rank : ""}
                </span>
              </CharacterDetails>
              <CharacterDetails>
                {character.attack_type} House:{" "}
                <span>{character.house}</span>
              </CharacterDetails>
              <CharacterDetails>
                {character.attack_type} Books:{" "}
                <span>
                  {character.books && character.books.length > 0
                    ? character.books.join(", ")
                    : "-"}
                </span>
              </CharacterDetails>
              <CharacterDetails>
                {character.attack_type} Titles:{" "}
                <span>
                  {character.titles && character.titles.length > 0
                    ? character.titles.join(", ")
                    : "-"}
                </span>
              </CharacterDetails>
            </CharacterInfoboxHeading>
          </CharacterInfoboxInner>
        </CharacterInfobox>
      )}
    </Wrapper>

  );
};

export default DetailView;

const Wrapper = styled("div")`
  position: relative;
  width: 50%;
`;

const CharacterInfobox = styled("div")`
  position: relative;
  background: rgba(0, 0, 0, 0.9);
  overflow: hidden;
  border-radius: 8px;
  color: #fefefe;
`;

const CharacterInfoboxBlurBackground = styled("img")`
  position: absolute;
  top: -12.5%;
  left: -12.5%;
  width: 125%;
  height: 125%;
  filter: blur(25px);
  object-fit: cover;
  opacity: 0.35;
  background-repeat: no-repeat;
  z-index: 1;
`;

const CharacterInfoboxInner = styled("div")`
  display: flex;
  flex-direction: column;
  align-items: center;
  position: relative;
  padding: 3rem;
  box-shadow: rgba(0, 0, 0, 0.25) 0px 0px 125px inset;
  z-index: 2;

  @media (min-width: 992px) {
    flex-direction: row;
  }
`;

const CharacterInfoboxImage = styled("img")`
  display: block;
  flex-shrink: 0;
  //width: 180px;
  //height: 128px;
  box-shadow: rgba(0, 0, 0, 0.3) 0px 12px 32px;
  object-fit: cover;
  border-radius: 16px;
  border-width: 1px;
  border-style: solid;
  border-color: rgba(0, 0, 0, 0.3);
  border-image: initial;
`;

const CharacterInfoboxHeading = styled("div")`
  flex: 1 1 100%;
  margin: 1.5rem 0 0;
  text-align: center;

  @media (min-width: 992px) {
    margin: 0 1.5rem;
    text-align: left;
  }
`;

const CharacterName2 = styled("h1")`
  margin: 0;
  color: #fefefe;
  font-weight: 500;
`;

const CharacterDetails = styled("p")`
  margin: 0.5rem 0 0;
  color: #fefefe;
  font-size: 0.8rem;
  letter-spacing: 1px;
  text-transform: uppercase;

  & span {
    color: #fefefe;
  }
`;
