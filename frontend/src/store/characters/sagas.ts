import { all, call, fork, put, takeEvery } from "redux-saga/effects";
import { callApi } from "../../utils/api";
import { fetchError, fetchSuccess, selected } from "./actions";
import { CharactersActionTypes } from "./types";

const API_ENDPOINT = "http://localhost:8080/api/v1";

function* handleFetch(page?: any) {
  try {
    // To call async functions, use redux-saga"s `call()`.
    const res = yield call(
      callApi,
      "get",
      API_ENDPOINT,
      `/characters?page=${page && page.payload && page.payload.page ? page.payload.page : ""}&text=${
      page && page.payload && page.payload.textToFind ? page.payload.textToFind : ""
      }`,
    );

    if (res.error) {
      yield put(fetchError(res.error));
    } else {
      yield put(fetchSuccess(res));
    }
  } catch (err) {
    if (err instanceof Error && err.stack) {
      yield put(fetchError(err.stack));
    } else {
      yield put(fetchError("An unknown error occured."));
    }
  }
}

function* handleFetchById(param: any) {
  try {
    // To call async functions, use redux-saga"s `call()`.
    const res = yield call(callApi, "get", API_ENDPOINT, `/characters/${param.payload}`);

    if (res.error) {
      yield put(fetchError(res.error))
    } else {
      yield put(selected(res.length >= 0 ? res[0] : null));
    }
  } catch (err) {
    if (err instanceof Error && err.stack) {
      yield put(fetchError(err.stack));
    } else {
      yield put(fetchError("An unknown error occured."));
    }
  }
}

function* watchFetchRequest() {
  yield takeEvery(CharactersActionTypes.FETCH_REQUEST, handleFetch);
}

function* watchFetchByIdRequest() {
  yield takeEvery(CharactersActionTypes.SELECT_HERO, handleFetchById);
}

function* charactersSaga() {
  yield all([fork(watchFetchRequest), fork(watchFetchByIdRequest)]);
}

export default charactersSaga;
