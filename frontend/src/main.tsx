import { ConnectedRouter } from "connected-react-router";
import { History } from "history";
import * as React from "react";
import { Provider } from "react-redux";
import { Store } from "redux";
import LayoutContainer from "./containers/LayoutContainer";
import Routes from "./routes";
import { IApplicationState } from "./store";

interface IMainProps {
  store: Store<IApplicationState>;
  history: History;
}

const Main: React.FC<IMainProps> = ({ store, history }) => {
  return (
    <Provider store={store}>
      <ConnectedRouter history={history}>
        <LayoutContainer>
          {({ }) => {
            return (
              <Routes />
            );
          }}
        </LayoutContainer>
      </ConnectedRouter>
    </Provider>
  );
};

export default Main;
