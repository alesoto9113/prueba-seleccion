"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
Object.defineProperty(exports, "__esModule", { value: true });
const characterModelDB_1 = require("../models/characterModelDB");
class CharacterControllerDB {
    create(character) {
        return __awaiter(this, void 0, void 0, function* () {
            const item = new characterModelDB_1.CharacterModelDB(character);
            yield item.save();
        });
    }
    list(pLimit, numberPage, text) {
        return __awaiter(this, void 0, void 0, function* () {
            const predicate = {
                $or: [
                    { name: { $regex: text, $options: "i" } },
                    { house: { $regex: text, $options: "i" } },
                ]
            };
            const records = yield characterModelDB_1.CharacterModelDB.collection.countDocuments(predicate);
            const actualPage = (numberPage >= 0 ? numberPage : 0);
            const items = yield characterModelDB_1.CharacterModelDB.find(predicate, "name gender house age pagerank")
                .skip((pLimit * actualPage)).limit(pLimit);
            return { data: items, pages: Math.floor((records / pLimit)), actual: actualPage, textToFind: text };
        });
    }
    find(id) {
        return __awaiter(this, void 0, void 0, function* () {
            const items = yield characterModelDB_1.CharacterModelDB.find({ _id: id });
            return items;
        });
    }
    dropCollection() {
        return __awaiter(this, void 0, void 0, function* () {
            yield characterModelDB_1.CharacterModelDB.collection.drop();
        });
    }
}
exports.CharacterControllerDB = CharacterControllerDB;
//# sourceMappingURL=characterControllerDB.js.map