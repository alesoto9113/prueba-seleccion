import * as React from "react";
import styled from "../../utils/styled";

interface IRootProps {
  className?: string;
}

const Root: React.SFC<IRootProps> = ({ children }) => <Wrapper>{children}</Wrapper>;

export default Root;

const Wrapper = styled("div")`
  display: flex;
  flex-direction: column;
  width: 100%;
  min-height: 100vh;
  background-color: #ebebea;
  color: #2e2e2c;
  font-family: "-apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell, Fira Sans, Droid Sans, Helvetica Neue, Arial, sans-serif";
`;
