import { connectRouter, RouterState } from "connected-react-router";
import { History } from "history";
import { Action, AnyAction, combineReducers, Dispatch } from "redux";
import { all, fork } from "redux-saga/effects";
import { charactersReducer } from "./characters/reducer";
import charactersSaga from "./characters/sagas";
import { ICharactersState } from "./characters/types";

export interface IApplicationState {
  characters: ICharactersState;
  router: RouterState;
}

export interface IConnectedReduxProps<A extends Action = AnyAction> {
  dispatch: Dispatch<A>;
}

export const createRootReducer = (history: History) =>
  combineReducers({
    characters: charactersReducer,
    router: connectRouter(history),
  });

export function* rootSaga() {
  yield all([fork(charactersSaga)]);
}
