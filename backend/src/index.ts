import config from "dotenv";
import mongoServer from "./config/db";
import server from "./config/server";

config.config();
const port = process.env.PORT || 8000;

server.listen(port, () => {
  // tslint:disable-next-line:no-console
  console.log(`Server started at http://localhost:${port}`);
  mongoServer.connect();
});
