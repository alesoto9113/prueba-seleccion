import { createHashHistory } from "history";
import * as React from "react";
import * as ReactDOM from "react-dom";
import configureStore from "./configureStore";
import Main from "./main";
import * as serviceWorker from "./serviceWorker";

import "typeface-ibm-plex-sans";
import "./styles";

const history = createHashHistory()
const initialState = window.initialReduxState;
const store = configureStore(history, initialState);

ReactDOM.render(<Main store={store} history={history} />, document.getElementById("root"));

serviceWorker.unregister();
