"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const cors_1 = __importDefault(require("cors"));
const http_status_1 = __importDefault(require("http-status"));
const HttpException_1 = __importDefault(require("../api/exceptions/HttpException"));
const options = {
    credentials: false,
    origin: (origin, callback) => {
        const whiteList = ["localhost"];
        const index = whiteList.findIndex((aWhiteListedOrigin) => origin.includes(aWhiteListedOrigin));
        if (!origin || index !== -1) {
            callback(null, true);
        }
        else {
            callback(new HttpException_1.default(http_status_1.default.FORBIDDEN, `'${origin}' is not allowed to access the specified route/resource`), false);
        }
    },
};
exports.default = () => cors_1.default(options);
//# sourceMappingURL=cors.js.map