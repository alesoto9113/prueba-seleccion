import * as React from "react";
import { connect } from "react-redux";
import DetailView from "../components/DetailView";
import FindField from "../components/FindField";
import Container from "../components/layout/Container";
import DataTable from "../components/layout/DataTable";
import Page from "../components/layout/Page";
import LoadingSpinner from "../components/LoadingSpinner";
import { IApplicationState, IConnectedReduxProps } from "../store";
import { fetchByIdRequest, fetchRequest } from "../store/characters/actions";
import { ICharacter } from "../store/characters/types";
import styled from "../utils/styled";

interface IPropsFromState {
  loading: boolean;
  data: ICharacter[];
  errors?: string;
  pages: number;
  actual: number;
  textToFind: string;
}

interface IPropsFromDispatch {
  fetchRequest: typeof fetchRequest;
  fetchByIdRequest: typeof fetchByIdRequest;
  selected: ICharacter;
}

interface IState {
  selected?: ICharacter;
  inputValue: string;
}

type AllProps = IPropsFromState & IPropsFromDispatch & IConnectedReduxProps;

class CharactersIndexPage extends React.Component<AllProps, IState> {
  constructor(props: AllProps) {
    super(props);
    const { fetchRequest: fr } = this.props;
    fr();
    this.state = {
      inputValue: "",
    };
  }

  public render() {
    const { loading, selected } = this.props;
    const character = selected;
    return (
      <Page>
        <Container>
          <TableWrapper>
            <FindField
              value={this.state.inputValue}
              onChange={this.updateInputValue}
              handleSubmit={this.handleSubmit.bind(this)}
            />
            {loading && (
                  <LoadingSpinner />
            )}
            {this.renderData()}

            {this.renderPaging()}
          </TableWrapper>
          <DetailView character={character} />
        </Container>
      </Page>
    );
  }

  private handleSubmit = e => {
    e.preventDefault();
    this.props.fetchRequest(0, this.state.inputValue);
  }

  private updateInputValue = e => {
    this.setState({
      inputValue: e.target.value,
    });
  }

  private renderData() {
    const { loading, data, actual } = this.props;
    const handleClick = (e: {
      preventDefault: () => void;
      currentTarget: { dataset: { value: any } };
    }) => {
      e.preventDefault();
      this.props.fetchByIdRequest(e.currentTarget.dataset.value);
    };
    return (
      <DataTable
        columns={["Index", "Name", "House", "Age", "Gender"]}
        widths={["auto", "", ""]}
      >
        {loading && data.length === 0 && (
          <CharacterLoading>
            <td colSpan={3}>Loading...</td>
          </CharacterLoading>
        )}
        {data.map((hero, index) => (
          <tr key={index}>
            <CharacterDetail>
              <CharacterName>

                <a
                  data-value={hero._id}
                  onClick={handleClick.bind(this)}
                  href="#" key={index}  >
                  {10 * actual + (index + 1)}
                </a>

              </CharacterName>
            </CharacterDetail>
            <td>{hero.name || ""}</td>
            <td>{hero.house || ""}</td>
            <td>{(hero.age ? hero.age.age : "-") || ""}</td>
            <td>{hero.gender || ""}</td>
          </tr>
        ))}
      </DataTable>
    );
  }

  private renderPaging() {
    const { pages, textToFind } = this.props;

    const handleClick = (e: {
      preventDefault: () => void;
      currentTarget: { dataset: { value: number } };
    }) => {
      e.preventDefault();
      this.props.fetchRequest(e.currentTarget.dataset.value - 1, textToFind);
    };

    const items = [];
    let i = 1;

    while (i <= pages + 1) {
      items.push(
        <PagingItem key={i}>
          <a
            href="#"
            data-value={i.toString()}
            onClick={handleClick.bind(this)}
            key={i}
          >
            {i}
          </a>
        </PagingItem>,
      );
      i++;
    }
    return <PagingWrapper>{items}</PagingWrapper>;
  }
}

const mapStateToProps = ({ characters }: IApplicationState) => ({
  actual: characters.actual,
  data: characters.data,
  errors: characters.errors,
  loading: characters.loading,
  pages: characters.pages,
  selected: characters.selected,
  textToFind: characters.textToFind,
});

const mapDispatchToProps = {
  fetchByIdRequest,
  fetchRequest,
};

export default connect(
  mapStateToProps,
  mapDispatchToProps,
)(CharactersIndexPage);

const PagingWrapper = styled("div")`
  display: flex;
  flex-wrap: wrap;
  margin-top: 1.5rem;
`;

const PagingItem = styled("div")`
  padding: 5px;
`;

const TableWrapper = styled("div")`
  position: relative;
  width:50%;
`;

const CharacterDetail = styled("td")`
  display: flex;
  flex-direction: row;
  align-items: center;
`;

const CharacterName = styled("div")`
  flex: 1 1 auto;
  height: 100%;

  a {
    color: #eb5558;
  }
`;

const CharacterLoading = styled("tr")`
  td {
    height: 48px;
    text-align: center;
  }
`;
