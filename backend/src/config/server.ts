import express from "express";
import routes from "../api/routes/v1";
import cors from "./cors";

class Server {
    public app: express.Application;
    constructor() {
        this.app = express();
        this.app.use((req, _, next) => {
            req.headers.origin = req.headers.origin || req.headers.host;
            next();
        });
        this.app.use(cors());
        this.app.use(express.json());
        this.app.use( express.urlencoded({ extended: true }) );
        this.app.use("/api/v1", routes);
    }
}

export default new Server().app;
