import * as React from "react";
import { Route, Switch } from "react-router-dom";
import Header from "./components/layout/Header";
import Root from "./components/layout/Root";
import CharactersPage from "./pages/characters";
import IndexPage from "./pages/index";

const Routes: React.SFC = () => (
  <Root>
    <Header title="GOT App" />
    <Switch>
      <Route exact path="/" component={IndexPage} />
      <Route path="/characters" component={CharactersPage} />
      <Route component={() => <div>Not Found</div>} />
    </Switch>
  </Root>
);

export default Routes;
