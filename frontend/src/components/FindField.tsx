import * as React from "react";
import styled from "../utils/styled";

export interface IFindFieldProps {
  handleSubmit: any;
  value: any;
  onChange: any;
}

const FindField: React.SFC<IFindFieldProps> = ({ handleSubmit, value, onChange }) => {
  return (
    <FindWrapper>
      <form onSubmit={handleSubmit}>
        <input type="text" value={value} onChange={onChange} name="name" placeholder="Find by name and house" />
        <input type="submit" value="Submit" />
      </form>
    </FindWrapper>
  );
};

const FindWrapper = styled("div")`
  float: right;
  margin-bottom: 10px;
`;

export default FindField;
