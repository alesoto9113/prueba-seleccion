"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const db_1 = __importDefault(require("./config/db"));
const server_1 = __importDefault(require("./config/server"));
dotenv_1.default.config();
const port = process.env.PORT || 8000;
server_1.default.listen(port, () => {
    // tslint:disable-next-line:no-console
    console.log(`Server started at http://localhost:${port}`);
    db_1.default.connect();
});
//# sourceMappingURL=index.js.map