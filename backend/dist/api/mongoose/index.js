"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const mongoose_1 = __importDefault(require("mongoose"));
class MongoServer {
    connect() {
        // tslint:disable-next-line:max-line-length
        mongoose_1.default.connect("mongodb+srv://asoto:asoto!@cluster0-43o5y.mongodb.net/got?retryWrites=true&w=majority", { useNewUrlParser: true, useFindAndModify: false });
        mongoose_1.default.connection.on("open", () => {
            // tslint:disable-next-line:no-console
            console.info("Connected to Mongo.");
        });
        mongoose_1.default.connection.on("error", (err) => {
            // tslint:disable-next-line:no-console
            console.error(err);
        });
    }
    disconnet() {
        mongoose_1.default.disconnect();
    }
}
exports.default = new MongoServer();
//# sourceMappingURL=index.js.map