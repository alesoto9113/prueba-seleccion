"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const dotenv_1 = __importDefault(require("dotenv"));
const mongoose_1 = __importDefault(require("mongoose"));
dotenv_1.default.config();
const DB_HOST = process.env.DB_HOST;
const DB_USER = process.env.DB_USER;
const DB_PASS = process.env.DB_PASS;
const DB_COLLECTION = process.env.DB_COLLECTION;
class MongoServer {
    connect() {
        // tslint:disable-next-line:max-line-length
        mongoose_1.default.connect(`mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_COLLECTION}?retryWrites=true&w=majority`, { useNewUrlParser: true, useFindAndModify: false });
        mongoose_1.default.connection.on("open", () => {
            // tslint:disable-next-line:no-console
            console.info("Connected to Mongo.");
        });
        mongoose_1.default.connection.on("error", (err) => {
            // tslint:disable-next-line:no-console
            console.error(err);
        });
    }
}
exports.default = new MongoServer();
//# sourceMappingURL=db.js.map