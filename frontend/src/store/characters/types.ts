export interface ICharacter extends ApiResponse {
  _id: string;
  name: string;
  gender: string;
  books: string[];
  titles: string[];
  slug: string;
  house: string;
}

export type ApiResponse = Record<string, any>;

export const enum CharactersActionTypes {
  FETCH_REQUEST = "@@characters/FETCH_REQUEST",
  FETCH_SUCCESS = "@@characters/FETCH_SUCCESS",
  FETCH_ERROR = "@@characters/FETCH_ERROR",
  SELECT_HERO = "@@characters/SELECT_HERO",
  SELECTED = "@@characters/SELECTED",
}


export interface ICharactersState {
  readonly loading: boolean;
  readonly data: ICharacter[];
  readonly errors?: string;
  readonly selected: ICharacter;
  readonly pages: number;
  readonly actual: number;
  readonly textToFind: string;
}
