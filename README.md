# Prueba de Experiencia (FullStack)
Nombre: Alejandro Soto Gonzalez
Email: alesoto9113@gmail.com

## Para correr servidor backend
```sh
npm i
npm run start
```

## Para iniciar frontend
```sh
npm i
npm run start
```

## Nota
En el backend existe un endpoint para extrarer la data de la API y cargarla a una base de datos mongodb free tier hosteada en [https://cloud.mongodb.com](https://cloud.mongodb.com)
Para acceder desde un cliente a la base de datos estos son los datos :
HOST: cluster0-43o5y.mongodb.net
USER: asoto
PASS: asoto!


