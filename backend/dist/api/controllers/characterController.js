"use strict";
var __awaiter = (this && this.__awaiter) || function (thisArg, _arguments, P, generator) {
    return new (P || (P = Promise))(function (resolve, reject) {
        function fulfilled(value) { try { step(generator.next(value)); } catch (e) { reject(e); } }
        function rejected(value) { try { step(generator["throw"](value)); } catch (e) { reject(e); } }
        function step(result) { result.done ? resolve(result.value) : new P(function (resolve) { resolve(result.value); }).then(fulfilled, rejected); }
        step((generator = generator.apply(thisArg, _arguments || [])).next());
    });
};
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const axios_1 = __importDefault(require("axios"));
const http_status_1 = __importDefault(require("http-status"));
const lodash_1 = __importDefault(require("lodash"));
const HttpException_1 = __importDefault(require("../exceptions/HttpException"));
const characterControllerDB_1 = require("../mongoose/controllers/characterControllerDB");
const db = new characterControllerDB_1.CharacterControllerDB();
class CharacterController {
    static get getInstance() {
        return this.instance || (this.instance = new this());
    }
}
CharacterController.listCharacters = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        const data = yield db.list(10, (req && req.query && req.query.page ? req.query.page : 0), (req && req.query && req.query.text ? req.query.text : ""));
        res.json(data);
    }
    catch (error) {
        res.status(http_status_1.default.BAD_REQUEST).json(new HttpException_1.default(http_status_1.default.BAD_REQUEST, "Error"));
    }
});
CharacterController.findCharacterById = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    try {
        const data = yield db.find(req && req.params && req.params.id ? req.params.id : "");
        res.json(data);
    }
    catch (error) {
        res.status(http_status_1.default.BAD_REQUEST).json(new HttpException_1.default(http_status_1.default.BAD_REQUEST, "Error"));
    }
});
CharacterController.getGotApiData = (req, res, next) => __awaiter(this, void 0, void 0, function* () {
    axios_1.default.get("https://api.got.show/api/general/characters")
        .then((response) => __awaiter(this, void 0, void 0, function* () {
        const { data } = response;
        const unionData = lodash_1.default.unionBy(data.book, data.show, "name");
        yield db.dropCollection();
        yield Promise.all(unionData.map((value, index) => {
            return db.create(value);
        }));
        res.json(data);
    }))
        .catch((error) => {
        res.status(http_status_1.default.BAD_REQUEST).json(new HttpException_1.default(http_status_1.default.BAD_REQUEST, "Error"));
    });
});
exports.default = CharacterController;
//# sourceMappingURL=characterController.js.map