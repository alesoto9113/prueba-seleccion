import * as mongoose from "mongoose";
const Schema = mongoose.Schema;
export const CharacterModelName = "Character";
import { ICharacterDB } from "../interfaces/ICharacterDB";

export const CharacterSchema = new Schema<ICharacterDB>({
    age: {
        age: {
            type: mongoose.Schema.Types.Number,
        },
        name: {
            type: mongoose.Schema.Types.String,
        },
    },
    image: {
        type: mongoose.Schema.Types.String,
    },
    name: {
        type: mongoose.Schema.Types.String,
    },

    pagerank: {
        rank: {
            type: mongoose.Schema.Types.Number,
        },
        title: {
            type: mongoose.Schema.Types.String,
        },
    },
    // tslint:disable-next-line:object-literal-sort-keys
    gender: {
        type: mongoose.Schema.Types.String,
    },
    slug: {
        type: mongoose.Schema.Types.String,
    },
    house: {
        type: mongoose.Schema.Types.String,
    },
    books: [
        { type: mongoose.Schema.Types.String },
    ],
    titles: [
        { type: mongoose.Schema.Types.String },
    ],
});

const CharacterModelDB = mongoose.model("Character", CharacterSchema);

export { CharacterModelDB };
