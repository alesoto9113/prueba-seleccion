import config from "dotenv";
import mongoose from "mongoose";

config.config();

const DB_HOST = process.env.DB_HOST;
const DB_USER = process.env.DB_USER;
const DB_PASS = process.env.DB_PASS;
const DB_COLLECTION = process.env.DB_COLLECTION;
class MongoServer {
    public connect() {
        // tslint:disable-next-line:max-line-length
        mongoose.connect(`mongodb+srv://${DB_USER}:${DB_PASS}@${DB_HOST}/${DB_COLLECTION}?retryWrites=true&w=majority`, { useNewUrlParser: true, useFindAndModify: false});
        mongoose.connection.on("open", () => {
            // tslint:disable-next-line:no-console
            console.info("Connected to Mongo.");
        });
        mongoose.connection.on("error", (err: any) => {
            // tslint:disable-next-line:no-console
            console.error(err);
        });
    }
}

export default new MongoServer();
