import * as mongoose from "mongoose";

export interface ICharacterDB extends mongoose.Document {
    _id: mongoose.Schema.Types.ObjectId;
    name: string;
    image: string;
    gender: string;
    slug: string;
    rank: string;
    house: string;
    books: string[];
    titles: string[];
    age: IAge;
    pagerank: IPageRank;
}
