import { CharacterModelDB } from "../models/characterModelDB";

export class CharacterControllerDB {

    public async create(character: ICharacter): Promise<void> {
        const item = new CharacterModelDB(character);
        await item.save();
    }

    public async list(pLimit: number, numberPage?: number, text?: string): Promise<any> {
        const predicate = {
            $or: [
                { name: {$regex: text, $options: "i"} },
                { house: {$regex: text, $options: "i"} },
            ]};
        const records: number = await CharacterModelDB.collection.countDocuments(predicate);
        const actualPage: number = (numberPage >= 0 ? numberPage : 0);
        const items: any = await CharacterModelDB.find(predicate, "name gender house age pagerank")
        .skip((pLimit * actualPage)).limit(pLimit);
        return {data: items, pages: Math.floor((records / pLimit)), actual: actualPage, textToFind: text};
    }

    public async find(id: string): Promise<any> {
        const items: any = await CharacterModelDB.find({_id: id});
        return items;
    }

    public async dropCollection(): Promise<void> {
        await CharacterModelDB.collection.drop();
    }

}
