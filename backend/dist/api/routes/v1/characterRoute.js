"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const characterController_1 = __importDefault(require("../../controllers/characterController"));
const router = express_1.default.Router();
router.route("/characters").get(characterController_1.default.listCharacters);
router.route("/characters/:id").get(characterController_1.default.findCharacterById);
router.route("/data").post(characterController_1.default.getGotApiData);
exports.default = router;
//# sourceMappingURL=characterRoute.js.map