import * as React from "react";
import styled from "../../utils/styled";

interface IDataTableProps {
  columns: string[]
  widths?: string[]
}

const DataTable: React.SFC<IDataTableProps> = ({ children, widths, columns }) => (
  <Wrapper>
    <thead>
      <tr>
        {columns.map((column, i) => (
          <th key={column} style={widths && widths[i] ? { width: widths[i] } : undefined}>
            {column}
          </th>
        ))}
      </tr>
    </thead>
    <tbody>{children}</tbody>
  </Wrapper>
)

export default DataTable;

const Wrapper = styled("table")`
  margin-bottom: 0;
  border-top: 1px solid #ebebea;
  border-bottom: 1px solid #ebebea;

  thead {
    tr {
      th {
        padding: 1rem;
        text-align: left;
        border-bottom: 2px solid #ebebea;
      }
    }
  }

  tbody {
    tr {
      border-top: 1px solid #ebebea;

      &:nth-child(even) {
        background: #ebebea;
      }

      td {
        padding: 0.5rem 1rem;
        font-size: 0.85rem;
      }
    }
  }
`;
