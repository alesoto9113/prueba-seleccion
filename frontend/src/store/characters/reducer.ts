import { Reducer } from "redux";
import { CharactersActionTypes, ICharactersState } from "./types";

export const initialState: ICharactersState = {
  actual: 0,
  data: [],
  errors: undefined,
  loading: false,
  pages: 0,
  textToFind: ""
};

const reducer: Reducer<ICharactersState> = (state = initialState, action) => {
  switch (action.type) {
    case CharactersActionTypes.FETCH_REQUEST: {
      return { ...state, loading: true };
    }
    case CharactersActionTypes.FETCH_SUCCESS: {
      return {
        ...state,
        actual: action.payload.actual,
        data: action.payload.data,
        loading: false,
        pages: action.payload.pages,
        textToFind: action.payload.textToFind
      };
    }
    case CharactersActionTypes.FETCH_ERROR: {
      return { ...state, loading: false, errors: action.payload };
    }
    case CharactersActionTypes.SELECT_HERO: {
      return { ...state, loading: false, selected: null };
    }
    case CharactersActionTypes.SELECTED: {
      return { ...state, loading: false, selected: action.payload };
    }
    default: {
      return state;
    }
  }
};

export { reducer as charactersReducer };
