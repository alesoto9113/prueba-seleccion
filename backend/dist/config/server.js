"use strict";
var __importDefault = (this && this.__importDefault) || function (mod) {
    return (mod && mod.__esModule) ? mod : { "default": mod };
};
Object.defineProperty(exports, "__esModule", { value: true });
const express_1 = __importDefault(require("express"));
const v1_1 = __importDefault(require("../api/routes/v1"));
const cors_1 = __importDefault(require("./cors"));
class Server {
    constructor() {
        this.app = express_1.default();
        this.app.use((req, _, next) => {
            req.headers.origin = req.headers.origin || req.headers.host;
            next();
        });
        this.app.use(cors_1.default());
        this.app.use(express_1.default.json());
        this.app.use(express_1.default.urlencoded({ extended: true }));
        this.app.use("/api/v1", v1_1.default);
    }
}
exports.default = new Server().app;
//# sourceMappingURL=server.js.map