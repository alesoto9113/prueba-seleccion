import cors from "cors";
import httpStatus from "http-status";
import HttpException from "../api/exceptions/HttpException";

const options: cors.CorsOptions = {
    credentials : false,
    origin: (origin, callback) => {
        const whiteList = ["localhost"];
        const index = whiteList.findIndex((aWhiteListedOrigin) => origin.includes(aWhiteListedOrigin));
        if (!origin || index !== -1) {
            callback(null, true);
        } else {
            callback(new HttpException(httpStatus.FORBIDDEN, `'${origin}' is not allowed to access the specified route/resource`), false);
        }
    },
};

export default () => cors(options);
