import * as React from "react";
import Container from "../components/layout/Container";
import Page from "../components/layout/Page";
import styled from "../utils/styled";

export default () => (
  <Page>
    <Container>
      <PageContent>
        <h1>Fullstack selection test</h1>
        <span><strong>Stack:</strong> React + Redux + Saga, Express, MongoDB</span>
      </PageContent>
    </Container>
  </Page>
);

const PageContent = styled("article")`
  max-width: 768px;
  padding: 10px;
  margin: 0 auto;
  line-height: 1.6;

  a {
    color: #eb5558;
  }

  h1,
  h2,
  h3,
  h4 {
    margin-bottom: 0.5rem;
    font-family: "'IBM Plex Sans', -apple-system, BlinkMacSystemFont, Segoe UI, Roboto, Oxygen, Ubuntu, Cantarell,  Fira Sans, Droid Sans, Helvetica Neue, Arial, sans-serif";
    line-height: 1.25;
  }
`;
