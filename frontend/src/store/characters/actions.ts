import { action } from "typesafe-actions";
import { CharactersActionTypes, ICharacter } from "./types";

// tslint:disable-next-line:max-line-length
export const fetchRequest = (page?: number, textToFind?: string) => action(CharactersActionTypes.FETCH_REQUEST, { page, textToFind });
export const fetchByIdRequest = (id: string) => action(CharactersActionTypes.SELECT_HERO, id);

export const fetchSuccess = (data: ICharacter[]) => action(CharactersActionTypes.FETCH_SUCCESS, data);
export const fetchError = (message: string) => action(CharactersActionTypes.FETCH_ERROR, message);
export const selected = (character: ICharacter) => action(CharactersActionTypes.SELECTED, character);
