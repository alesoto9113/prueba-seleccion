import express from "express";
import controller from "../../controllers/characterController";
const router = express.Router();

router.route("/characters").get(controller.listCharacters);
router.route("/characters/:id").get(controller.findCharacterById);
router.route("/data").post(controller.getGotApiData);

export default router;
