interface ICharacter {
    titles: string[];
    books: string[];
    _id: string;
    id: string;
    name: string;
    slug: string;
    image: string;
    gender: string;
    house: string;
    pagerank: IPageRank;
    age: IAge;
}
