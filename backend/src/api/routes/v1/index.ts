import express from "express";
import characterRoutes from "./characterRoute";

const router = express.Router();

router.use("/", characterRoutes);

export default router;
